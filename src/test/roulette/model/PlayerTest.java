package roulette.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 *
 */
public class PlayerTest {

    @Test
    public void update_when_bet_is_win() {

        Player player = new Player("James", 1.0, 2.0);

        BetResult betResult = new BetResult(Outcome.WIN, new BigDecimal(20.0));

        Bet curBet = Bet.BetBuilder.getInstnace().setAmount( new BigDecimal(10.0)).setType(BetType.EVEN).build();

        player.setCurBet(curBet);

        player.update(betResult);

        assertThat(player.getTotalWin().doubleValue(), is(21.0));
        assertThat(player.getTotalBet().doubleValue(), is(12.0));
    }

    @Test
    public void update_when_bet_is_lose() {

        Player player = new Player("James", 1.0, 2.0);

        BetResult betResult = new BetResult(Outcome.LOSE, new BigDecimal(0.0));

        Bet curBet = Bet.BetBuilder.getInstnace().setAmount( new BigDecimal(10.0)).setType(BetType.EVEN).build();

        player.setCurBet(curBet);

        player.update(betResult);

        assertThat(player.getTotalWin().doubleValue(), is(1.0));
        assertThat(player.getTotalBet().doubleValue(), is(12.0));
    }

    @Test
    public void update_when_cur_bet_is_null() {

        Player player = new Player("James", 1.0, 2.0);

        BetResult betResult = new BetResult(Outcome.LOSE, new BigDecimal(0.0));

        player.update(betResult);

        assertThat(player.getTotalWin().doubleValue(), is(1.0));
        assertThat(player.getTotalBet().doubleValue(), is(2.0));
    }


}
