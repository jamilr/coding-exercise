package roulette.io;

import org.junit.Test;
import roulette.io.console.ConsoleCommand;
import roulette.io.console.ConsoleCommandException;
import roulette.io.console.ConsoleCommandExecutor;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 *
 */
public class ConsoleCommandExecutorTest {

    @Test(expected = ConsoleCommandException.class)
    public void execute_invalid_command() throws ConsoleCommandException {

        ConsoleCommand consoleCommand = ConsoleCommand.NA;
        ConsoleCommandExecutor.execute(consoleCommand);
    }
}
