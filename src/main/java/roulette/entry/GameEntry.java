package roulette.entry;

import org.apache.log4j.net.SyslogAppender;
import roulette.core.Gameplay;
import roulette.io.DataReaderException;
import roulette.io.InputDataReader;
import roulette.model.Player;
import roulette.util.ErrorMessages;
import org.apache.log4j.Level;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * GameEntry -- the main class that starts the game application from the command line
 *
 */
public class GameEntry {

    /**
     *
     * the main method that starts the application up
     *
     * */
    public static void main(String[] args) {

        String filePath = GameOptionsParser.parse(args);

        Map<String, Player> playerMap = readPlayers(filePath);

        Gameplay gameplay = new Gameplay(playerMap);

        gameplay.start();
    }

    /**
     * readPlayers() -- a helper method that reads the players' details from the file
     *
     * @param filePath -- the file path to the file with players' details
     *
     * Application terminates, if the file path is invalid
     *
     * */
    private static Map<String, Player> readPlayers(String filePath) {

        Map<String, Player> playerMap = new HashMap<>();

        try {

            playerMap = InputDataReader.readPlayers(filePath);

        } catch (IOException io) {

            System.out.println(ErrorMessages.FAILURE_ON_READING_PLAYERS_FILE);
            System.exit(-1);
        } catch (DataReaderException dataReaderException){

            System.exit(-1);
        }

        return playerMap;
     }
}
