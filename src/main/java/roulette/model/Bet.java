package roulette.model;

import java.math.BigDecimal;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * Bet class represents a single bet player makes while playing a game
 * It consists of three key properties
 *
 * - betType (BetType) - the type fo the bet,
 * - number (int) - the number a player wants to bet on
 * - amount (BigDecimal) - amount of money a player wants to bet
 *
 *
 *
 */
public class Bet {

    private final BetType betType;
    private final int number;
    private final BigDecimal amount;

    protected Bet(BetBuilder betBuilder) {
        this.number = betBuilder.number;
        this.amount = betBuilder.amount;
        this.betType = betBuilder.type;
    }

    public BetType getBetType() {
        return betType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public int getNumber() {
        return number;
    }

    /**
     *
     * BetBuilder implements the Builder design pattern to create immutable Bet object
     *
     * */
    public static class BetBuilder {

        private BetType type;

        private int number;

        private BigDecimal amount;

        BetBuilder() {
        }

        public BetBuilder setType(BetType type) {
            this.type = type;
            return this;
        }

        public BetBuilder setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public BetBuilder setNumber(int number) {
            this.number = number;
            return this;
        }

        public static BetBuilder getInstnace() {

            return new BetBuilder();
        }

        public Bet build() {

            return new Bet(this);
        }
    }
}
