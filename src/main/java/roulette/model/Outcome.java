package roulette.model;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * Outcome -- represents the outcome of the bet given the roulette iteration
 *
 * - WIN -- a player wins
 * - LOSE -- a player losses
 */
public enum Outcome {
    WIN,
    LOSE
}
