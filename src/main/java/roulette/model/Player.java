package roulette.model;

import java.math.BigDecimal;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * Player -- represents a single player in the game
 *
 * - name -- name of the player
 * - totalWin -- the total win a player won so far
 * - totalBet -- the toal bet a player bet on so far
 * - curBet -- the current bet a player put
 * - curBetResult -- the result af the roulette iteration against the current player's bet
 *
 */
public class Player {

    private String name;

    private BigDecimal totalWin;

    private BigDecimal totalBet;

    private Bet curBet;

    private BetResult curBetResult;

    public Player(String playerName, double totalWin, double totalBet) {

        this.name = playerName;

        this.totalWin = new BigDecimal(totalWin);

        this.totalBet = new BigDecimal(totalBet);
    }

    public String getName() {
        return name;
    }

    public BigDecimal getTotalWin() {
        return totalWin;
    }

    public BigDecimal getTotalBet() {
        return totalBet;
    }

    /**
     * update() -- updates the totalWin, and totalBet properties for a player given the result of the roulette iteration
     *
     * @param betResult -- result of roulette iteration given the player's bet
     *
     * */
    public void update(BetResult betResult) {

        if (curBet == null) return;

        totalBet = totalBet.add(curBet.getAmount());

        Outcome outcome = betResult.getOutcome();

        switch (outcome) {

            case WIN: {

                totalWin = totalWin.add( betResult.getWin());
            }
            break;
            default:
        }

        this.curBetResult = betResult;
    }

    public Bet getCurBet() {
        return curBet;
    }

    public void setCurBet(Bet curBet) {
        this.curBet = curBet;
    }

    public BetResult getCurBetResult() {
        return curBetResult;
    }
}
