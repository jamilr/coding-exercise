package roulette.util;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * Constants - the constants used by the application
 *
 */
public class Constants {

    public static final String NUM_TOKEN = "Number: ";

    public static final String SPACE = " ";
    public static final String COMMA = ",";

    public static final int PERIOD = 30000;
}
