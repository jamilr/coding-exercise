package roulette.util;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * TokenParser is a helper class that checks the value for particular type
 *
 */
public class TokenParser {

    public static final String FLOAT_TOKEN = "[-+]?[0-9]*\\.?[0-9]+";

    public static final String INT_TOKEN = "\\d+";

    /**
     *
     * isFloat() -- a helper method that checks whether the given value is of Float type (real value)
     *
     */
    public static boolean isFloat(String value) {

        return value.matches(FLOAT_TOKEN);
    }

    /**
     * isInt() -- a helper method that checks whether the given value is of integer type (integer value)
     *
     * */
    public static boolean isInt(String strValue) {

        return  strValue.matches(INT_TOKEN);
    }
}
