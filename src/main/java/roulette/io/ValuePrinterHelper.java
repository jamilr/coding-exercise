package roulette.io;

import java.util.Arrays;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * ValuePrinterHelper() -- a helper class that creates a string object with a provided string value
 *
 *
 * In order to print the bet results as well as total player's win and bet statistics,
 *
 * every section in the message has a fixed number of characters
 *
 * For the player name section it is 20 as a total number of characters to be printed
 *
 * For any other section, it is 15 as a total number of character to be printered
 *
 */
public class ValuePrinterHelper {

    public enum ValueType {
        PLAYER_NAME,
        VALUE
    }

    private static final int PLAYER_NAME_TEMP_SIZE = 20;

    private static final int VALUE_TEMP_SIZE = 15;

    /**
     * printValue() -- inserts the String value inside the template char array as to create a String with a fixed number
     * of characters to use it in printing bet results or total players' win and bet statistics
     *
     * @param type -- a value type of the String value
     * @param value -- the value that needs to be inserted into template char array
     *
     * */
    public static String insertValue(ValueType type, String value) {

        char[] newValueCharArray;

        int size = (type.equals(ValueType.PLAYER_NAME)) ? PLAYER_NAME_TEMP_SIZE : VALUE_TEMP_SIZE;

        newValueCharArray = new char[size];

        Arrays.fill(newValueCharArray, ' ');

        if (value != null) {

            int i = 0;
            while (i < value.length()) {
                newValueCharArray[i] = value.charAt(i);
                i++;
            }
        }

        return new String(newValueCharArray);
    }

}
