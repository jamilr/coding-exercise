package roulette.io;

import roulette.io.console.ConsoleCommandException;
import roulette.model.Player;
import roulette.model.util.BetParser;
import roulette.model.util.BetParserException;
import roulette.model.util.BetParserResult;
import roulette.util.GameMessages;

import java.util.*;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * BetReader -- reads the bets for all the players in the game
 *
 */
public class BetReader {

    public static Map<String, Player> read(Map<String, Player> playerMap) {

        System.out.println(GameMessages.BET_MESSAGE);

        Scanner scanner = new Scanner(System.in);

        Set<String> playerNameSet = new HashSet<>(playerMap.keySet());

        while (true) {

            try {

                String playerBet = scanner.nextLine();

                BetParserResult betResult = BetParser.parse(playerBet);

                String playerName = betResult.getPlayer();

                if (!playerNameSet.contains(playerName))
                    if (playerMap.keySet().contains(playerName))
                        System.out.println(GameMessages.PLAYER_GOT_NEW_BET);
                    else {
                        System.out.println(GameMessages.PROVIDED_PLAYER_DOES_NOT_EXIST);
                        continue;
                    }

                playerMap.get(playerName).setCurBet(betResult.getBet());

                playerNameSet.remove(playerName);

                if (playerNameSet.isEmpty()) {
                    System.out.println(GameMessages.BET_INPUT_ACKNOWLEDGEMENT);
                    break;
                }

            } catch (BetParserException betException) {

                System.out.println(betException.getMessage());
                System.out.println(GameMessages.PLEASE_TRY_AGAIN_TO_ENTER_BET);
            } catch (ConsoleCommandException consoleEx) {

                System.out.println(consoleEx.getMessage());
            }
        }

        return playerMap;
    }
}
