package roulette.core;

import roulette.model.BetResult;
import roulette.model.BetResultHolder;
import roulette.model.Player;
import roulette.io.BetReader;
import roulette.io.ResultPrinter;
import roulette.util.Constants;

import java.util.Map;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * PlayerManager class implements the logic for managing the players
 *
 * When the game starts, the PlayerManager is waiting for alll bets for all the users to be provided from the command line
 *
 * As soon as the bets provided, it then waits for the result to arrive at betResultHolder from Roulette object
 *
 * PlayerManager is an adaptation of Consumer in the Producer-Consumer design pattern
 *
 * Using wait(), notifyAll() methods, the application follows intra-thread communication protocol between Roulette and PlayerManager
 * that notifies PlayerManager object about a newly generated roulette result given the new bets are provided by players
 *
 */
public class PlayerManager implements Runnable {

    private BetResultHolder betResultHolder;

    private Map<String, Player> playerMap;

    private ResultPrinter resultPrinter;

    private BetResultProvider betResultProvider;

    public PlayerManager(Map<String, Player> playerMap, BetResultHolder game) {

        this.betResultHolder = game;

        this.playerMap = playerMap;

        this.betResultProvider = new BetResultProvider();

        this.resultPrinter = new ResultPrinter();
    }

    public void run() {

        try {

            while (true) {

                if (betResultHolder.getNewBetSet()) continue;

                BetReader.read(playerMap);

                betResultHolder.setNewBetSet(true);

                while (true) {

                    synchronized (betResultHolder) {

                        while ( betResultHolder.getResult() == null)
                            betResultHolder.wait();
                    }

                    int result;

                    synchronized (betResultHolder) {

                        result = betResultHolder.getResult();

                        betResultHolder.setNewBetSet(false);
                        betResultHolder.setResult(null);
                    }

                    for (String playerName : playerMap.keySet()) {

                        BetResult betResult = betResultProvider.calc(playerMap.get(playerName).getCurBet(), result);
                        playerMap.get(playerName).update(betResult);
                    }

                    System.out.println(Constants.NUM_TOKEN + result);

                    resultPrinter.print(playerMap);

                    break;
                }
            }

        } catch (InterruptedException interEx) {

        }
    }
}
