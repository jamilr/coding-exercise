package roulette.core;

import roulette.model.BetResultHolder;
import roulette.util.Constants;
import roulette.util.RandomNumGen;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * Roulette class implements the roulette logic
 *
 * Every 30 seconds it produces a random integer value in the rnage (1 - 36)
 * In case, there is a new bet provided by a player, and betResultHolder.getNewBetSet() is true, then,
 * Roulette assigns a newly generated random integer value to the bet result holder (betResultHolder) and notifies a PlayerManager thread
 * PlayerManager is waiting for a new roulette value to arrive at betResultHolder to process the resuult and print the outomes for all the players
 *
 * The Roulette is an adaptation of Producer class in the Producer-Consumer design pattern
 *
 * Using wait(), notifyAll() methods, the application follows intra-thread communication protocol between Roulette and PlayerManager
 * that notifies PlayerManager object about a newly generated roulette result given the new bets are provided by players
 *
 */
public class Roulette implements Runnable {

    private BetResultHolder betResultHolder;

    private RandomNumGen randomNumGen;

    public Roulette(BetResultHolder game) {
        this.betResultHolder = game;
        this.randomNumGen = new RandomNumGen();
    }

    public void run() {

        try {

            while (true) {

                Thread.currentThread().sleep(Constants.PERIOD);

                int result = randomNumGen.getNew();

                if (betResultHolder.getNewBetSet()) {

                    synchronized (betResultHolder) {

                        betResultHolder.setResult(result);
                        betResultHolder.notifyAll();
                    }
                }
            }

        } catch (InterruptedException in) {
        }
    }
}
